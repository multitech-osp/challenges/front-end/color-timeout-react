import { useState, useEffect } from "react";
import "./styles.css";

const Item = ({ timeout, color }) => {
  const [pulse, setPulse] = useState(true);

  let className = "item";

  if (pulse) {
    className += " pulse";
  }

  useEffect(() => {
    setInterval(() => {
      setPulse(false);
    }, timeout);
  }, [timeout]);

  return <div style={{ backgroundColor: color }} className={className} />;
};

export default Item;
