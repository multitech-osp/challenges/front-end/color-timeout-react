import "./styles.css";
import { useState, useEffect } from "react";
import data from "./data.json";
import Item from "./Item";

/*
    Para essa tarefa, deve ser utilizado os dados
    do json importado como data.

    A cada renderização do componente Item,
    deve ser aguardado o timeout para inclusão
    do próximo item no array de items.

    A cada renderização o timeout deve ser somado
    no estado time.
*/

const App = () => {
  const [items] = useState([]);
  const [time] = useState(0);

  return (
    <div className="wrapper">
      <h1>Time: {time}</h1>
      {items.map(({ color, timeout }) => (
        <Item key={color} timeout={timeout} color={color} />
      ))}

      <Item
        key={data[0].color}
        timeout={data[0].timeout}
        color={data[0].color}
      />
      <Item
        key={data[1].color}
        timeout={data[1].timeout}
        color={data[1].color}
      />
      <Item
        key={data[2].color}
        timeout={data[2].timeout}
        color={data[2].color}
      />
    </div>
  );
};

export default App;
