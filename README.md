# color-timeout-react

## Instruções

Utilize apenas o arquivo _App.js_ para execução das tarefas.

Esperamos o resultado abaixo:

![project image](./color-timeout.gif)

Para essa tarefa, deve ser utilizado os dados
do json importado como data.

A cada renderização do componente Item,
deve ser aguardado o timeout para inclusão
do próximo item no array de items.

A cada renderização o timeout deve ser somado
no estado time.
